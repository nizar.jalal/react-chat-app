import React from 'react';


const AvatarColor = (email) => {

    const colors = ['#00AA55', '#009FD4', '#B381B3', '#939393', '#E3BC00', '#D47500', '#DC2A2A'];

    function numberFromText(text) {
      // numberFromText("AA");
      const charCodes = text
        .split('') // => ["A", "A"]
        .map(char => char.charCodeAt(0)) // => [65, 65]
        .join(''); // => "6565"
      return parseInt(charCodes, 10);
    }
    const firstDigit = email.email[0]
    let backgroundColor = colors[numberFromText(firstDigit) % colors.length]; // => "#DC2A2A"

    const style = {
            width: 52,
            height: 52,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: backgroundColor,
            borderRadius: '50%',
            fontFamily: 'sans-serif',
            color: '#fff',
            fontWeight: 'bold',
            fontSize: 16
    }

    return(
        <div style={{display: "inline-flex",fontWeight: 'bold',}}>
            <div id="FillAvatar" style={style}>{firstDigit.toUpperCase()}</div>
            <div style={{display:'flex', flexDirection: 'column', justifyContent:'center', marginLeft: 10}}>
        <div>{email.email}</div>
            </div>
        </div>
        
    )
}

export default AvatarColor;